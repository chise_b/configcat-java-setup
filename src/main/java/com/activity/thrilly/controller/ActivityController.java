package com.activity.thrilly.controller;

import com.activity.thrilly.domain.Activity;
import com.activity.thrilly.service.BoredAPIService;
import com.configcat.ConfigCatClient;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ActivityController {

    private static final ConfigCatClient client = new ConfigCatClient("<YOU-SDK-KEY-HERE>");

    @Autowired
    private BoredAPIService boredAPIService;

    private List<Activity> getActualActivities(Integer nrOfActivities){
        List<Activity> activities;
        Boolean isFeatureEnabled = client.getValue(Boolean.class, "isMyFirstFeatureEnabled", true);
        if (isFeatureEnabled){
            activities = boredAPIService.getRandomRecommendedActivities(nrOfActivities);
        }
        else{
            activities = boredAPIService.getRandomActivities(nrOfActivities);
        }
        return activities;
    }

    @GetMapping("/activities/{nrOfActivities}")
    public ResponseEntity<List<Activity>> getActivities(@PathVariable Integer nrOfActivities) {
        if (nrOfActivities > 5){
            return new ResponseEntity("You can ask for maximum of 5 activities!", HttpStatus.BAD_REQUEST);
        }

        List<Activity> activities = getActualActivities(nrOfActivities);
        if (activities.size() == nrOfActivities){
            return ResponseEntity.ok(activities);
        }

        return ResponseEntity.noContent().build();
    }

}
