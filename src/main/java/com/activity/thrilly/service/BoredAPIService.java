package com.activity.thrilly.service;

import com.activity.thrilly.domain.Activity;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class BoredAPIService {

    private static final String BORED_API_BASE_URL = "http://www.boredapi.com/api";
    private static OkHttpClient httpClient = new OkHttpClient();
    private final Logger logger = LoggerFactory.getLogger(BoredAPIService.class);
    private Gson gson = new Gson();

    public List<Activity> getRandomRecommendedActivities(int numberOfActivities) {
        List<Activity> activities = getRandomActivities(numberOfActivities);
        if (activities.size() > 0){
            Activity activity = activities.get(0);
            activity.setRecommended(true);
        }
        return activities;
    }

    public List<Activity> getRandomActivities(int numberOfActivities) {
        List<Activity> activities = new ArrayList<>();
        for (int currentActivityNr = 0; currentActivityNr < numberOfActivities; currentActivityNr++) {
            Request request = new Request.Builder()
                .url(BORED_API_BASE_URL + "/activity")
                .build();

            Call call = httpClient.newCall(request);
            try {
                Response response = call.execute();
                if (response.body() != null) {
                    Activity activity = gson.fromJson(response.body().string(), Activity.class);
                    activities.add(activity);
                }
            } catch (IOException e) {
                logger.info("There was an error when trying to find activities!");
                e.printStackTrace();
            }
        }
        return activities;
    }


}
